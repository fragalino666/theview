$(document).ready(function() { 

	var carousel = $('.block_carousel .carousel').owlCarousel({
		center: true,
		loop: true,
		dots: false,
		responsive: {
			768: {
				items: 3,
			},
			0: {
				items: 2,
			}
		}
	});

	$('.text-slider').owlCarousel({
		loop: true,
		dots: true,
		items: 1,
		margin: 20
	});

	$('.block_carousel .prev').on('click', function(){
		carousel.trigger('prev.owl.carousel');
	});

	$('.block_carousel .next').on('click', function(){
		carousel.trigger('next.owl.carousel');
	});

});